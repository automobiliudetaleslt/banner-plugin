<?php

namespace Omni\Sylius\BannerPlugin\Model;

use Sylius\Component\Core\Model\ImageInterface;

interface BannerImageInterface extends ImageInterface
{
    /**
     * @return string
     */
    public function getLink(): ?string;

    /**
     * @param $link
     *
     * @return BannerImageInterface
     */
    public function setLink($link): BannerImageInterface;
}
