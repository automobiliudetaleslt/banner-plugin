<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Resource\Model\ToggleableInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;

interface BannerInterface extends
    ResourceInterface,
    ImagesAwareInterface,
    TimestampableInterface,
    CodeAwareInterface,
    ToggleableInterface
{
    /**
     * @return \DateTime
     */
    public function getPublishFrom();

    /**
     * @param \DateTime $publishFrom
     *
     * @return BannerInterface
     */
    public function setPublishFrom($publishFrom);

    /**
     * @return \DateTime
     */
    public function getPublishTo();

    /**
     * @param \DateTime $publishTo
     *
     * @return BannerInterface
     */
    public function setPublishTo($publishTo);

    /**
     * @param \DateTime|null $date
     *
     * @return bool
     */
    public function isPublished(\DateTime $date = null);

    /**
     * @return BannerPosition
     */
    public function getPosition();

    /**
     * @param BannerPosition $position
     *
     * @return BannerInterface
     */
    public function setPosition($position);
}
