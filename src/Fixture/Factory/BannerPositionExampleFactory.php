<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Fixture\Factory;

use Doctrine\ORM\EntityManager;
use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Omni\Sylius\BannerPlugin\Model\BannerPositionTranslationInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitAddressInterface;
use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitChannelSettingsInterface;
use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\AbstractExampleFactory;
use Sylius\Bundle\CoreBundle\Fixture\Factory\ExampleFactoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Formatter\StringInflector;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerPositionExampleFactory extends AbstractExampleFactory implements ExampleFactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $positionFactory;

    /**
     * @var EntityRepository
     */
    private $zoneRepository;

    /**
     * @var EntityRepository
     */
    private $localeRepository;

    /**
     * @var OptionsResolver
     */
    private $optionsResolver;

    /**
     * @param FactoryInterface $positionFactory
     * @param EntityRepository $zoneRepository
     * @param EntityRepository $localeRepository
     */
    public function __construct(
        FactoryInterface $positionFactory,
        EntityRepository $zoneRepository,
        EntityRepository $localeRepository
    ) {
        $this->positionFactory = $positionFactory;
        $this->zoneRepository = $zoneRepository;
        $this->localeRepository = $localeRepository;
        $this->optionsResolver = new OptionsResolver();

        $this->configureOptions($this->optionsResolver);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $options = []): BannerPositionInterface
    {
        $options = $this->optionsResolver->resolve($options);

        /** @var BannerPositionInterface $position */
        $position = $this->positionFactory->createNew();
        /** @var BannerZoneInterface $zone */
        $zone = $this->zoneRepository->findOneBy(['code' => $options['zone']]);

        if (!$zone) {
            throw new \Exception('Banner zone "' . $options['zone'] . '" not found');
        }

        $position->setCode($options['code']);
        $position->setZone($zone);
        $position->setPriority(0);
        $position->setType($options['type']);

        /** @var LocaleInterface $locale */
        foreach ($this->localeRepository->findAll() as $locale) {
            /** @var BannerPositionTranslationInterface $translation */
            $translation = $position->getTranslation($locale->getCode());
            $translation->setTitle($options['title']);
            $translation->setDescription($options['description']);
            $translation->setLink($options['link']);
        }

        return $position;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired([
                'code',
                'zone',
                'type',
                'title',
            ])
            ->setDefault('link', '')
            ->setDefault('description', '')
        ;
    }
}
