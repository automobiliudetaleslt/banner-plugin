<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Fixture;

use Sylius\Bundle\CoreBundle\Fixture\AbstractResourceFixture;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class BannerFixture extends AbstractResourceFixture
{
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'omni_banner';
    }

    /**
     * {@inheritdoc}
     */
    protected function configureResourceNode(ArrayNodeDefinition $resourceNode): void
    {
        $resourceNode
            ->children()
                ->scalarNode('code')->cannotBeEmpty()->end()
                ->scalarNode('position')->cannotBeEmpty()->end()
                ->arrayNode('channels')->prototype('scalar')->end()->end()
                ->scalarNode('published_from')->cannotBeEmpty()->end()
                ->scalarNode('published_to')->cannotBeEmpty()->end()
                ->arrayNode('images')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('image')->cannotBeEmpty()->end()
                            ->scalarNode('content_space')->end()
                            ->scalarNode('content_background')->end()
                            ->scalarNode('content')->end()
                            ->scalarNode('title')->end()
                            ->scalarNode('link')->end()
                            ->scalarNode('position')->defaultValue(0)->end()
                            ->enumNode('content_position')
                                ->cannotBeEmpty()
                                ->values(
                                    [
                                        'default',
                                        'left',
                                        'right',
                                        'top',
                                        'bottom',
                                    ]
                                )
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
