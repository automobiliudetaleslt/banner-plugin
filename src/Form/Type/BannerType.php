<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Form\Type;

use InvalidArgumentException;
use Omni\Sylius\BannerPlugin\Constraint\BannerImage;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerPositionRepository;
use Omni\Sylius\BannerPlugin\Model\Banner;
use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZone;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime as DateTimeConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class BannerType extends AbstractResourceType
{
    /**
     * @var BannerPositionRepository
     */
    private $positionRepository;

    /**
     * @var string
     */
    private $positionClass;

    /**
     * @param string   $dataClass
     * @param string[] $validationGroups
     * @param BannerPositionRepository $positionRepository
     * @param string   $positionClass
     */
    public function __construct(
        string $dataClass,
        array $validationGroups = [],
        BannerPositionRepository $positionRepository,
        string $positionClass
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->positionRepository = $positionRepository;
        $this->positionClass = $positionClass;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var BannerZone $zone */
        $zone = $options['zone'];

        if (!$zone instanceof BannerZoneInterface) {
            throw new InvalidArgumentException('Expected an instance of banner zone');
        }

        $builder
            ->add(
                'enabled',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'sylius.ui.enabled',
                    'constraints' => new Type('bool'),
                ]
            )
            ->add(
                'publishFrom',
                DateTimeType::class,
                [
                    'date_widget' => 'single_text',
                    'time_widget' => 'single_text',
                    'label' => 'omni_sylius.form.publish_from',
                    'required' => false,
                    'constraints' => new DateTimeConstraint(),
                ]
            )
            ->add(
                'publishTo',
                DateTimeType::class,
                [
                    'date_widget' => 'single_text',
                    'time_widget' => 'single_text',
                    'label' => 'omni_sylius.form.publish_to',
                    'required' => false,
                    'constraints' => new DateTimeConstraint(),
                ]
            )
            ->add(
                'position',
                EntityType::class,
                [
                    'label' => 'omni_sylius.form.position',
                    'class' => $this->positionClass,
                    'choices' => $this->positionRepository->findByZone($zone),
                    'choice_label' => function (BannerPositionInterface $position) {
                        return $position->getTitle();
                    },
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => BannerImageType::class,
                    'allow_add' => true,
                    'required' => false,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'sylius.ui.image',
                ]
            )
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var Banner $banner */
                $banner = $event->getData();
                if (!$banner instanceof $this->dataClass) {
                    throw new InvalidArgumentException('Expected an instance of banner');
                }

                $event->getForm()
                    ->add(
                        'code',
                        TextType::class,
                        [
                            'label' => 'sylius.ui.code',
                            'disabled' => null !== $banner->getCode(),
                            'constraints' => new NotBlank(),
                        ]
                    );
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(
            [
                'constraints' => [new UniqueEntity(['code']), new BannerImage()],
            ]
        );

        $resolver->setRequired('zone');
        $resolver->setAllowedValues(
            'zone',
            function ($value) {
                return $value instanceof BannerZoneInterface;
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'omni_sylius_banner';
    }
}
