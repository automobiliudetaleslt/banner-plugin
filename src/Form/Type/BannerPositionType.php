<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Form\Type;

use Omni\Sylius\BannerPlugin\Form\Subscriber\AddZoneSubscriber;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class BannerPositionType extends AbstractResourceType
{
    /**
     * @var AddZoneSubscriber
     */
    private $addZoneSubscriber;

    /**
     * @param string $dataClass
     * @param array $validationGroups
     * @param AddZoneSubscriber $addZoneSubscriber
     */
    public function __construct(
        string $dataClass,
        array $validationGroups = [],
        AddZoneSubscriber $addZoneSubscriber
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->addZoneSubscriber = $addZoneSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var BannerZoneInterface $zone */
        $zone = $options['zone'];

        if (!$zone instanceof BannerZoneInterface) {
            throw new \InvalidArgumentException('Expected an instance of banner zone');
        }

        $builder
            ->add(
                'code',
                TextType::class,
                [
                    'label' => 'sylius.ui.code',
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'sylius.ui.title',
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'width',
                IntegerType::class,
                [
                    'label' => 'omni_sylius.form.width',
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'height',
                IntegerType::class,
                [
                    'label' => 'omni_sylius.form.height',
                    'constraints' => new NotBlank(),
                ]
            )
        ;

        $builder->addEventSubscriber($this->addZoneSubscriber);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('zone');
        $resolver->setAllowedValues(
            'zone',
            function ($value) {
                return $value instanceof BannerZoneInterface;
            }
        );
    }
}
