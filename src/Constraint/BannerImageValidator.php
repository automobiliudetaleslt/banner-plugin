<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Constraint;

use Omni\Sylius\BannerPlugin\Model\Banner;
use Omni\Sylius\BannerPlugin\Model\BannerImage as BannerImageEntity;
use Omni\Sylius\BannerPlugin\Model\BannerPosition;
use SplFileInfo;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BannerImageValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof BannerImage) {
            throw new UnexpectedTypeException($constraint, BannerImage::class);
        }

        if (!$value instanceof Banner
            || !$value->getImage() instanceof BannerImageEntity
            || !$value->getImage()->getFile() instanceof SplFileInfo
            || !$value->getPosition() instanceof BannerPosition
        ) {
            return;
        }

        $position = $value->getPosition();
        $image = $value->getImage()->getFile();

        $imageConstraint = new Image(
            [
                'mimeTypes' => $constraint->mimeTypes,
                'allowSquare' => $constraint->allowSquare,
                'allowLandscape' => $constraint->allowLandscape,
                'allowPortrait' => $constraint->allowPortrait,
                'detectCorrupted' => $constraint->detectCorrupted,
                'maxHeight' => $position->getHeight(),
                'minHeight' => $position->getHeight(),
                'maxWidth' => $position->getWidth(),
                'minWidth' => $position->getWidth(),
                'mimeTypesMessage' => $constraint->mimeTypesMessage,
                'sizeNotDetectedMessage' => $constraint->sizeNotDetectedMessage,
                'maxWidthMessage' => $constraint->maxWidthMessage,
                'minWidthMessage' => $constraint->minWidthMessage,
                'maxHeightMessage' => $constraint->maxHeightMessage,
                'minHeightMessage' => $constraint->minHeightMessage,
                'maxRatioMessage' => $constraint->maxRatioMessage,
                'minRatioMessage' => $constraint->minRatioMessage,
                'allowSquareMessage' => $constraint->allowSquareMessage,
                'allowLandscapeMessage' => $constraint->allowLandscapeMessage,
                'allowPortraitMessage' => $constraint->allowPortraitMessage,
                'corruptedMessage' => $constraint->corruptedMessage,
                'binaryFormat' => $constraint->binaryFormat,
                'notFoundMessage' => $constraint->notFoundMessage,
                'notReadableMessage' => $constraint->notReadableMessage,
                'maxSizeMessage' => $constraint->maxSizeMessage,
                'disallowEmptyMessage' => $constraint->disallowEmptyMessage,
                'uploadIniSizeErrorMessage' => $constraint->uploadIniSizeErrorMessage,
                'uploadFormSizeErrorMessage' => $constraint->uploadFormSizeErrorMessage,
                'uploadPartialErrorMessage' => $constraint->uploadPartialErrorMessage,
                'uploadNoFileErrorMessage' => $constraint->uploadNoFileErrorMessage,
                'uploadNoTmpDirErrorMessage' => $constraint->uploadNoTmpDirErrorMessage,
                'uploadCantWriteErrorMessage' => $constraint->uploadCantWriteErrorMessage,
                'uploadExtensionErrorMessage' => $constraint->uploadExtensionErrorMessage,
                'uploadErrorMessage' => $constraint->uploadErrorMessage,
            ]
        );

        $validator = $this->context->getValidator()->inContext($this->context);
        $validator->atPath('images[0].file')->validate($image, $imageConstraint);
    }
}
