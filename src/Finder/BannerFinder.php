<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Finder;

use Omni\Sylius\BannerPlugin\Model\Banner;
use Omni\Sylius\BannerPlugin\Model\BannerImage;
use Omni\Sylius\BannerPlugin\Model\BannerPosition;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerRepository;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerZoneRepository;
use Sylius\Component\Resource\Factory\FactoryInterface;

/**
 * Class BannerFinder.
 */
class BannerFinder
{
    /**
     * @var BannerZoneRepository
     */
    private $zoneRepository;

    /**
     * @var BannerRepository
     */
    private $bannerRepository;

    /**
     * @var FactoryInterface
     */
    private $imageFactory;

    /**
     * @var FactoryInterface
     */
    private $bannerFactory;

    /**
     * @var Banner[][]
     */
    private $banners = [];

    /**
     * @var Banner[]
     */
    private $placeholders = [];

    /**
     * BannerFinder constructor.
     *
     * @param BannerZoneRepository $zoneRepository
     * @param BannerRepository     $bannerRepository
     * @param FactoryInterface     $imageFactory
     * @param FactoryInterface     $bannerFactory
     */
    public function __construct(
        BannerZoneRepository $zoneRepository,
        BannerRepository $bannerRepository,
        FactoryInterface $imageFactory,
        FactoryInterface $bannerFactory
    ) {
        $this->zoneRepository = $zoneRepository;
        $this->bannerRepository = $bannerRepository;
        $this->imageFactory = $imageFactory;
        $this->bannerFactory = $bannerFactory;
    }

    /**
     * @param string $positionCode
     * @param array  $options
     *
     * @return Banner
     */
    public function findForPosition(string $positionCode, array $options = []): Banner
    {
        return $this->getLastOrPlaceHolder($this->findAllForPosition($positionCode, $options), $positionCode);
    }

    /**
     * @param string $positionCode
     * @param array  $options
     *
     * @return Banner[]
     */
    public function findAllForPosition(string $positionCode, array $options = []): array
    {
        if (!isset($this->banners[$positionCode]) || !empty($options['no_cache'])) {
            $this->loadBanners($positionCode);
        }

        return $this->banners[$positionCode];
    }

    /**
     * @param string $position
     */
    private function loadBanners(string $position): void
    {
        $zone = $this->zoneRepository->findOneByPositionCode($position);

        foreach ($zone->getPositions() as $position) {
            $this->placeholders[$position->getCode()] = $this->getEmptyBanner($position);
            $this->banners[$position->getCode()] = [];
        }

        $banners = $this->bannerRepository->findForZone($zone);

        foreach ($banners as $banner) {
            $this->banners[$banner->getPosition()->getCode()][] = $banner;
        }
    }

    /**
     * @param BannerPosition $bannerPosition
     *
     * @return Banner
     */
    private function getEmptyBanner(BannerPosition $bannerPosition): Banner
    {
        /** @var BannerImage $image */
        $image = $this->imageFactory->createNew();
        /** @var Banner $banner */
        $banner = $this->bannerFactory->createNew();
        $banner->addImage($image);
        $banner->setPosition($bannerPosition);

        return $banner;
    }

    /**
     * @param Banner[] $banners
     * @param string   $positionCode
     *
     * @return Banner
     */
    private function getLastOrPlaceHolder(array $banners, string $positionCode): Banner
    {
        $last = end($banners);
        if (false === $last) {
            return $this->placeholders[$positionCode];
        }

        return $last;
    }
}
